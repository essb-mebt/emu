// File: SscanSubRoutines.c
// Author: Idoia Mazkiaran <imazkiaran@essbilbao.org>
//
// Functions related to the processing of the acquisition channel acquired in EMU

#include "SscanSubRoutines.h"
#include "SscanMode.h"



#ifdef TEST_PC
long GaussInit(struct _ioAssignChannel * precord) {
#else
long GaussInit(aSubRecord *precord) {
#endif
      double s, *p;
      long x;

      dataTest.slitMin  = 0;
      dataTest.slitMax  = 10; // Tests
      dataTest.slitAmp  = 10;
      dataTest.slitSigma= 25;
      dataTest.slitLoc  = 0x00;
      dataTest.slitOff  = 0x00;

      dataTest.gridMin   = 0x00;
      dataTest.gridMax   = 0x05; // Tests;
      dataTest.gridAmp   = 10;
      dataTest.gridSigma = 25;
      dataTest.gridLoc   = 0x00;
      dataTest.gridOff   = 0x00;

//      if (Initialized) return (0);

      dataTest.slitGauss = malloc((dataTest.slitMax-dataTest.slitMin) * sizeof(double));
      p = dataTest.slitGauss;
      s = 2.0 * dataTest.slitSigma * dataTest.slitSigma;
      for (x = dataTest.slitMin; x < dataTest.slitMax; x++) {
          *p = dataTest.slitAmp * exp(-( ((x - dataTest.slitLoc)*(x - dataTest.slitLoc))/s )) + dataTest.slitOff;
          p++;
      }


      dataTest.gridGauss = malloc((dataTest.gridMax-dataTest.gridMin) * sizeof(double));
      p = dataTest.gridGauss;
      s = 2.0 * dataTest.gridSigma * dataTest.gridSigma;
      for (x = dataTest.gridMin; x < dataTest.gridMax; x++) {
          *p = dataTest.gridAmp * exp(-( ((x - dataTest.gridLoc)*(x - dataTest.gridLoc))/s )) + dataTest.gridOff;
          p++;
      }

    return 0;
}


// For displaying average intensity in bars

#ifdef TEST_PC
long GaussProcess(struct _ioAssignChannel * precord) {
#else
long GaussProcess(aSubRecord *precord) {
#endif


#ifdef TEST_PC
#else
    *(long*)precord->vala = 0x00;
#endif
    return 0;
}



#ifdef TEST_PC
long channelsReadInit(struct _ioAssignChannel * precord) {
#else
long channelsReadInit(aSubRecord *precord) {
#endif


    return 0;
}



#ifdef TEST_PC
long channelsReadProcess(struct _ioAssignChannel * precord) {
#else
long channelsReadProcess(aSubRecord *precord) {
#endif


    double *a;
    double p[10];
    long b;

#ifdef TEST_PC
#else
    a = (double*)precord->a;  // num points slit movement

    b = (long*)precord->b;  //
#endif
//    memcpy(&p[0], a,  precord->noa * sizeof(float));
    memcpy(&p[0], a,  10 * sizeof(double));


/*
#ifdef TEST_PC
#else
//    *(long*)precord->vala = countSample;
#endif
*/
    return 0;
}



#ifdef TEST_PC
long gridScanDataInit(struct _ioAssignChannel * precord) {
#else
long gridScanDataInit(aSubRecord * precord) {
#endif

    memMask = 0x00;

    return 0;
}





#ifdef TEST_PC
long freeMemory(struct _ioAssignChannel * precord) {
#else
long freeMemory(aSubRecord * precord) {
#endif
#ifdef MEM_STORE
    free(intensityData);
    free(xData);
    free(xpData);
    free(avgData);
    memMask = 0x00;
#endif
     return 0;
}





#ifdef TEST_PC
long gridScanDataProcess(struct _ioAssignChannel * precord) {
#else
long gridScanDataProcess(aSubRecord *precord) {
#endif

    long i;
    double b;

#ifdef TEST_PC
    dataGrid.numChannels = 4;       // active Channels
    dataGrid.pointsSlit = 2;        // number positions of slit
    dataGrid.pointsGrid = 4;        // number positions of grid
    dataGrid.slitPosition = 1;      // slit position
    dataGrid.distSlitGrid = 400;     // distance slit to grid
    dataGrid.wiresDistance = 0.5;     // distance between wires
    dataGrid.countSlit = 1;        // num of slit positions already processed

#else
    b = *(double*)precord->a;  // grid positions
/*
    a = (double*)precord->a;  // grid positions

    dataGrid.slitPosition = *(float*)precord->b;     // slit position
    dataGrid.countSlit = *(long*)precord->c;        // actual position of slit
    dataGrid.numChannels = *(long*)precord->d;       // active Channels
    dataGrid.pointsSlit = *(long*)precord->e;        // number positions of slit
    dataGrid.pointsGrid = *(long*)precord->f;        // number positions of grid
    dataGrid.distSlitGrid = *(double*)precord->g;        // distance from slit to grid
    dataGrid.wiresDistance = *(double*)precord->h;        // distance wires
    dataGrid.scanMode = *(long*)precord->i;        // scan mode

    memcpy(&dataGrid.positionGrid[0], a,  dataGrid.pointsGrid * sizeof(double));
*/

#endif
    printf("gridScanDataProcess \n");
//    dataGrid.wiresDistance = 0.5;



    if (dataGrid.countSlit == 0x00) // Init, first movement
    {
//#ifdef MEM_STORE
// Allocate memory from this position


        printf("gridPosition %f\n", b);
//        printf("gridPosition %d\n", a);
        printf("slitPosition %d\n", dataGrid.slitPosition);
        printf("countSlit %d\n", dataGrid.countSlit);
        printf("numChannels %d\n", dataGrid.numChannels);
        printf("pointsSlit %f\n", dataGrid.pointsSlit);
        printf("pointsGrid %f\n", dataGrid.pointsGrid);

/*
        if (dataGrid.scanMode == PREDEFINED_SCAN)
        {
            memAllocate = filePointsLength;
            printf("memAllocate = filePointsLength \n");
        }
        else
            memAllocate = dataGrid.pointsSlit * dataGrid.pointsGrid;
        printf("memAllocate %l\n", memAllocate);
*/

        if (memMask)
        {
            if (memMask & MASK_MEMXDATA)
                free(xData);
            if (memMask & MASK_MEMXPDATA)
                free(xpData);
            if (memMask & MASK_MEMAVGDATA)
                free(avgData);
            if (memMask & MASK_MEMINTDATA)
                free(intensityData);
        }

        memAllocate = SCAN_POINTS_INTENSITY2D; // Tests

        memMask = 0x00;
        if((xData = (double*)malloc(memAllocate*WIRE_CNT * sizeof(double))) == NULL)
        {
            printf("Error allocating the storage for the pointer to buffers\n");
            return -1;
        }
        memMask |= MASK_MEMXDATA;
        if((xpData = (double*)malloc(memAllocate*WIRE_CNT * sizeof(double))) == NULL)
        {
            printf("Error allocating the storage for the pointer to buffers\n");
            return -1;
        }
        memMask |= MASK_MEMXPDATA;
        if((avgData = (double*)malloc(memAllocate*WIRE_CNT * sizeof(double))) == NULL)
        {
            printf("Error allocating the storage for the pointer to buffers\n");
            return -1;
        }
        memMask |= MASK_MEMAVGDATA;
        if((intensityData = (double*)malloc(memAllocate * sizeof(double))) == NULL)
        {
            printf("Error allocating the storage for the pointer to buffers\n");
            return -1;
        }
        memMask |= MASK_MEMINTDATA;


//        if((arrayAvg = (double*)malloc((WIRE_CNT+1) * sizeof(double))) == NULL)
//        {
//            printf("Error allocating the storage for the pointer to buffers\n");
//            return -1;
//        }




//#endif




        for (i=0; i< (SCAN_POINTS_INTENSITY2D*WIRE_CNT); i++)
        {
            xData[i] = 0x00;
            xpData[i] = 0x00;
            avgData[i] = 0x00;
        }

        for (i=0; i< (SCAN_POINTS_INTENSITY2D); i++)
        {
            intensityData[i] = 0x00;
        }

        for (i=0; i< (WIRE_CNT+1); i++)
        {
            arrayAvg[i] = 0x00;
        }

    }


    return 0;
}



#ifdef TEST_PC
long samplesCollectionInit(struct _ioAssignChannel * precord) {
#else
long samplesCollectionInit(aSubRecord * precord) {
#endif


    return 0;
}




#ifdef TEST_PC
long samplesCollectionProcess(struct _ioAssignChannel * precord) {
#else
long samplesCollectionProcess(aSubRecord *precord) {
#endif

    long nChannel;
    long i, j;

    double *a;


#ifdef TEST_PC
    nChannel = 1;          // num Channel to be stored
    dataGrid.numChannels = 4;       // active Channels
    dataGrid.pointsSlit = 2;        // number positions of slit
    dataGrid.pointsGrid = 4;        // number positions of grid
    dataGrid.slitPosition = 1;      // slit position
    dataGrid.distSlitGrid = 400;     // distance slit to grid
    dataGrid.wiresDistance = 0.5;     // distance between wires
    dataGrid.countSlit = 1;        // num of slit positions already processed


    for (i=0; i< dataGrid.pointsGrid; i++)
        dataGrid.currentGrid[i] = i;
    for (i=0; i< dataGrid.pointsGrid; i++)
        dataGrid.positionGrid[i] = i;

    dataGrid.positionGrid[0] = 1.800000;
    dataGrid.positionGrid[1] = 1.900000;
    dataGrid.positionGrid[2] = 2.000000;
    dataGrid.positionGrid[3] = 2.100000;
    dataGrid.positionGrid[4] = 2.200000;
    dataGrid.positionGrid[5] = 0.000000;
    dataGrid.positionGrid[6] = 0.000000;
    dataGrid.positionGrid[7] = 0.000000;

#else

    a = (double*)precord->a;  // num points slit movement

    memcpy(&dataGrid.currentGrid[0], a,  dataGrid.pointsGrid * sizeof(double));
    nChannel = *(long*) precord->b;  //

#endif

    printf("Channel[0] %6f\n", dataGrid.currentGrid[0]);
    printf("pointsGrid %d\n", dataGrid.pointsGrid);
    printf("countSlit %d\n", dataGrid.countSlit);
    printf("numChannels %d\n", dataGrid.numChannels);
    printf("nChannel %d\n", nChannel);

// In precalculated positions mode
    indexSamples = indexSamples + (nChannel * dataGrid.pointsGrid);

//    indexSamples = (dataGrid.countSlit * dataGrid.pointsGrid * dataGrid.numChannels) + (nChannel * dataGrid.pointsGrid); // So the points for a slit position are ordered by channel and grid position

    printf("indexSamples %d \n", indexSamples);

//  sscan precalculated positions mode
    if (dataGrid.scanMode == PREDEFINED_SCAN)
    {
        if (indexSamples > dataGrid.nTotalSamples) // Head of file, total number of samples, in the first position of slit line.
            indexSamples = 0;
    }
    else
    if (indexSamples > (dataGrid.pointsSlit * dataGrid.pointsGrid * dataGrid.numChannels - dataGrid.pointsGrid)) // double check
        indexSamples = 0;


    for (i = 0; i < dataGrid.pointsGrid; i++)
    {

         xData[indexSamples + i] = dataGrid.slitPosition;
         xpData[indexSamples + i] = (((dataGrid.positionGrid[i]) + dataGrid.wiresDistance * (nChannel + dataGrid.numChannels/2)) - dataGrid.slitPosition) / dataGrid.distSlitGrid;
         avgData[indexSamples + i] = dataGrid.currentGrid[i];

// Better to calculate date once all the x,y, x',y' values have been received
// It has to be checked, if the colum depends on the value of Xp
// For intensity, order the x and xp values, taken in account the precalculated position values.
/*
         if (dataGrid.scanMode == LINEAR_SCAN)
             indexIntensity = ((((nChannel * dataGrid.pointsGrid) + i) * dataGrid.pointsSlit) + dataGrid.countSlit);
         else
         {
             indexIntensity = ((((nChannel * dataGrid.pointsGrid) + i) * dataGrid.pointsSlit) + dataGrid.countSlit);
         }
//         indexIntensity = ((((nChannel * dataGrid.pointsGrid) + i) * dataGrid.pointsSlit) + dataGrid.countSlit);


         // InputData = P11, P12,...P1M, P21, P22, ... P2M, ... PN1, PN2, ...PNM
         //    The resulting image will display the array elements as follows:
         //    P11, P12, ... P1M
         //    P21, P22, ... P2M
         //         ...
         //    PN1, PN2, ... PNM

         intensityData[indexIntensity] = dataGrid.currentGrid[i];
*/
         // Channels Avg: for the Bar graphic
         if ((dataGrid.countSlit == 0x00) && (i == 0x00)) // First value of scanning
            arrayAvg[nChannel+1] = dataGrid.currentGrid[i];
         else
         {
            arrayAvg[nChannel+1] += dataGrid.currentGrid[i];
            arrayAvg[nChannel+1] /= 2;
         }
    }

    totalSamplesRead = indexSamples+dataGrid.pointsGrid;
#ifdef TEST_PC

#else
    printf("dataGrid.positionGrid[0] %6f \n", dataGrid.positionGrid[0]);
    printf("dataGrid.positionGrid[1] %6f \n", dataGrid.positionGrid[1]);
    printf("dataGrid.positionGrid[2] %6f \n", dataGrid.positionGrid[2]);
    printf("dataGrid.positionGrid[3] %6f \n", dataGrid.positionGrid[3]);

    printf("dataGrid.positionGrid[4] %6f \n", dataGrid.positionGrid[4]);
    printf("dataGrid.positionGrid[5] %6f \n", dataGrid.positionGrid[5]);
    printf("dataGrid.positionGrid[6] %6f \n", dataGrid.positionGrid[6]);
    printf("dataGrid.positionGrid[7] %6f \n", dataGrid.positionGrid[7]);

    printf("xData[0] %6f \n", xData[0]);
    printf("xData[1] %6f \n", xData[1]);

    printf("xpData[0] %6f \n", xpData[0]);
    printf("xpData[1] %6f \n", xpData[1]);

    j = totalSamplesRead * sizeof(double);

    precord->neva = j;
    precord->nevb = j;
    precord->nevc = j;

    memcpy(precord->vala,&xData[0],j);
    memcpy(precord->valb,&xpData[0],j);
    memcpy(precord->valc,&avgData[0],j);
    memcpy(precord->vald,&intensityData[0],1000*sizeof(double));

    memcpy(precord->vale,&arrayAvg[0],dataGrid.numChannels*sizeof(double));

    *(double*)precord->valf = indexSamples+dataGrid.pointsGrid;
#endif


    return 0;
}




#ifdef TEST_PC
long WireIntegrityInit(struct _ioAssignChannel * precord) {
#else
long WireIntegrityInit(aSubRecord *precord) {
#endif

    maskWire = 0x00000000;


    return 0;
}




#ifdef TEST_PC
long WireIntegrityProcess(struct _ioAssignChannel * precord) {
#else
long WireIntegrityProcess(aSubRecord *precord) {
#endif
    long nChannel, i, j, k, k2;
    double sampleRate, halfPeriod, signalFreq, signalAmp;
    double interSample;
    long nSamples, countH, countL;
    double *a;


    a = (double*)precord->a;

    nSamples = precord->noa;
    nChannel = *(long*)precord->b;
    memcpy(&wireIntegrityData[nChannel][0], a,  precord->noa * sizeof(double));
    sampleRate = *(double*)precord->c;
    signalFreq = *(double*)precord->d;
    signalAmp  = *(double*)precord->e;


//    wireIntegrityData for squared signal
/*
    halfPeriod = sampleRate / signalFreq;
    if (halfPeriod > nSamples) halfPeriod = nSamples;

    countH = 0x00;
    countL = 0x00;

    for (i=0,j=0; i<halfPeriod; i++,j++)
    {
        interSample = wireIntegrityData[nChannel][j];
        if ((interSample > (signalAmp * 0.5)) && (interSample < (signalAmp * 1.5)))
            countH++;
    }

    for (i=halfPeriod, j=halfPeriod; i<nSamples; i++,j++)
    {
        interSample = wireIntegrityData[nChannel][j];
        if (interSample < (signalAmp * 0.5))
            countL++;
    }


// If nChannel
    if ((countH > (0.8 * halfPeriod)) && (countH > (0.8 * halfPeriod)))
    {
        k = (0x1 << nChannel);
        maskWire |= k;
        *(long*)precord->valb = maskWire; // OK
    }
    else
    {
        k = (0x1 << nChannel);
        k2 = (maskWire & ~k);
        *(long*)precord->valb &= k2;
    }

*/
    //    wireIntegrityData for continuous signal
    countH = 0x00;

    for (i=0,j=0; i<nSamples; i++,j++)
    {
        interSample = wireIntegrityData[nChannel][j];
        if ((interSample > (signalAmp * 0.5)) && (interSample < (signalAmp * 1.5)))
            countH++;
    }

    if (countH > (nSamples/2)) // Wire OK
    {
        k = (0x1 << nChannel);
        maskWire |= k;
        *(long*)precord->valb = maskWire; // OK
    }
    else
    {
        k = (0x1 << nChannel);
        k2 = (maskWire & ~k);
        *(long*)precord->valb &= k2;
    }


    if ((*(long*)precord->valb & 0xFFFFFFFF) == 0xFFFFFFFF)
        *(long*)precord->vala = 0x01;
    else
        *(long*)precord->vala = 0x02;

    return 0;
}








/* Register these symbols for use by IOC code: */


#ifndef TEST_PC
epicsRegisterFunction(freeMemory);
epicsRegisterFunction(gridScanDataInit);
epicsRegisterFunction(gridScanDataProcess);
epicsRegisterFunction(samplesCollectionInit);
epicsRegisterFunction(samplesCollectionProcess);
epicsRegisterFunction(channelsReadInit);
epicsRegisterFunction(channelsReadProcess);
epicsRegisterFunction(WireIntegrityInit);
epicsRegisterFunction(WireIntegrityProcess);
#endif



