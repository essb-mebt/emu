// File: SscanMode.c
// Author: Idoia Mazkiaran <imazkiaran@essbilbao.org>
//
// Functions related to the reading of the file for precalculated values for motion in the EMU


#include "SscanMode.h"



#ifdef TEST_PC
long PrecalPositionsInit(struct _ioAssignChannel * precord) {
#else
long PrecalPositionsInit(aSubRecord *precord) {
#endif


    return 0;
}


// For displaying average intensity in bars

#ifdef TEST_PC
long PrecalPositionsProcess(struct _ioAssignChannel * precord) {
#else
long PrecalPositionsProcess(aSubRecord *precord) {
#endif

    FILE* fd = NULL;
    char *a;
    long b;
    char fileName[200];
    long c;
    long nLine, loop, bytes, flgLine;

    char *delim = " "; // input separated by spaces
    char *token = NULL;

    char *str = NULL;


    float buffPoints[512];
    long pBuff;

    char* s;

    printf(" PrecalPositionsProcess\n");

    const char moduleVble[] = "REQUIRE_slitgridscanner_PATH";
    const char moduleDirectory[] = "misc";

#ifdef TEST_PC
    b = 1;

    fd = fopen("/home/imazkiaran/idoia/code/sscan/sscan/text.txt","r");


#else
    a = (char*)precord->a;  // Name of file
    b = *(long*)precord->b; //

    printf("b %d \n", b);


    printf("environmVble %s \n", moduleVble);

    s = getenv(moduleVble);

    sprintf(fileName,"%s/%s/%s",s,moduleDirectory,a);

    printf("fileName %s \n", fileName);

//    fd = fopen(fileName,"r");
    fd = fopen("/opt/epics/modules/slitgridscanner/idoia/misc/test1.txt","r");


#endif

    if(NULL == fd)
    {
        printf("\n fopen() Error!!!\n");
        return 0;
    }
    else
    {
        bytes = 512;
        loop = 0;
        flgLine = 1;
#ifdef TEST_PC
        nLine = 0x00;
#else
        nLine = b;
#endif
        do {
            getline(&str, &bytes, fd);
            if (loop == nLine)
                flgLine = 0;
            loop++;

        }while(flgLine);


        printf("str %s \n", str);


        pBuff = 0x00;
        for (token = strtok(str, delim); token != NULL; token = strtok(NULL, delim))
        {
          char *unconverted;
          buffPoints[pBuff] = strtod(token, &unconverted);
          if (!isspace(*unconverted) && *unconverted != 0)
          {
              printf("Character not valid in file\n");


             // Input string contains a character that's not valid
             // in a floating point constant

          }
          pBuff++;
        }

        printf("pBuff %f \n", pBuff);

        printf("buffPoints[0] %f \n", buffPoints[0]);
        printf("buffPoints[1] %f \n", buffPoints[1]);
        printf("buffPoints[2] %f \n", buffPoints[2]);
        printf("buffPoints[3] %f \n", buffPoints[3]);



    #ifdef TEST_PC

    #else
        if (b == 0) // First row are slit positions
        {
            memcpy(precord->vala,buffPoints,pBuff*sizeof(float)); // Slit positions
            *(long*)precord->valb = pBuff; // Number of slit positions
        }
        memcpy(precord->valc,buffPoints,pBuff*sizeof(float)); // Grid positions
        *(long*)precord->vald = pBuff; // Number of grid positions
    #endif

        fclose(fd);

        } // else

        return 0;
}



#ifdef TEST_PC
long LengthPointsInit(struct _ioAssignChannel * precord) {
#else
long LengthPointsInit(aSubRecord *precord) {
#endif


    return 0;
}

#ifdef TEST_PC
double LengthPointsProcess(struct _ioAssignChannel * precord) {
#else
double LengthPointsProcess(aSubRecord *precord) {
#endif
    FILE* fd = NULL;
    char *a;
    long b;
    char fileName[200];
    long nLine, loop, bytes, flgLine;

    char *delim = " "; // input separated by spaces
    char *token = NULL;

    char *str = NULL;


    float buffPoints[512];
    long pBuff;

    char* s;

    double nPointsSlit, nPointsGrid;
    long nGridLines;

    printf(" PrecalPositionsProcess\n");

    const char moduleVble[] = "REQUIRE_slitgridscanner_PATH";
    const char moduleDirectory[] = "misc";

#ifdef TEST_PC

    fd = fopen("/home/imazkiaran/idoia/code/sscan/sscan/text.txt","r");


#else
    a = (char*)precord->a;  // Name of file
    b = *(long*)precord->b; //

    printf("b %d \n", b);


    printf("environmVble %s \n", moduleVble);

    s = getenv(moduleVble);

    sprintf(fileName,"%s/%s/%s",s,moduleDirectory,a);

    printf("fileName %s \n", fileName);

//    fd = fopen(fileName,"r");
    fd = fopen("/opt/epics/modules/slitgridscanner/idoia/misc/test1.txt","r");


#endif

    if(NULL == fd)
    {
        printf("\n fopen() Error!!!\n");
        return 0;
    }
    else
    {
        bytes = 512;
        loop = 0;
        flgLine = 1;
        nLine = 0x00; // Read first line with slit positions.

        do {
            getline(&str, &bytes, fd);
            if (loop == nLine)
                flgLine = 0;
            loop++;
        }while(flgLine);


        printf("str %s \n", str);


        pBuff = 0x00;
        for (token = strtok(str, delim); token != NULL; token = strtok(NULL, delim))
        {
          char *unconverted;
          buffPoints[pBuff] = strtod(token, &unconverted);
          if (!isspace(*unconverted) && *unconverted != 0)
          {
              printf("Character not valid in file\n");

             // Input string contains a character that's not valid
             // in a floating point constant
          }
          pBuff++;
        }

        nPointsSlit = pBuff;


/*
        nGridLines = 0x00;
        while (!feof(fd)) {
            getline(&str, &bytes, fd);
            nGridLines++;
        }

        if (nPointsSlit > nGridLines)
        {
            printf("\n File Format Error: check number of grid points lines!!!\n");
            fclose(fd);
            return 0;
        }
*/


        loop = 0;
        flgLine = 1;
        nLine = nPointsSlit-1; // Read the lines grid positions - slit positions line.
        nPointsGrid = 0x00;
        nGridLines = 0x00;

        do {
            getline(&str, &bytes, fd);
            if (loop == nLine)
                flgLine = 0;
            loop++;
            printf("str %s \n", str);

            pBuff = 0x00;
            for (token = strtok(str, delim); token != NULL; token = strtok(NULL, delim))
            {
              char *unconverted;
              buffPoints[pBuff] = strtod(token, &unconverted);
              if (!isspace(*unconverted) && *unconverted != 0)
              {
                  printf("Character not valid in file\n");


                 // Input string contains a character that's not valid
                 // in a floating point constant

              }
              pBuff++;
            }
            nPointsGrid += pBuff;
            nGridLines++;

        }while(/*(flgLine) &&*/ (!feof(fd)));

        if (feof(fd))
        {
            nGridLines--;
            nPointsGrid--;
        }
        if (nPointsSlit > nGridLines)
        {
            printf("\n File Format Error: check number of grid points lines!!!\n");
            fclose(fd);
            return 0;
        }


        fclose(fd);

        filePointsLength = nPointsSlit * nPointsGrid;

        return(0);

        } // else

}


#ifdef TEST_PC
double AllocateMemProcess(struct _ioAssignChannel * precord) {
#else
double AllocateMemProcess(aSubRecord * precord) {
#endif


#if 0
#ifdef MEM_STORE
    double lengthData;
    long i;
    char* a;

#ifdef TEST_PC
#else
    a = (char*)precord->a;  // Name of file

    dataGrid.pointsSlit = *(long*)precord->b;        // number positions of slit
    dataGrid.pointsGrid = *(long*)precord->c;        // number positions of grid
    dataGrid.numChannels = *(long*)precord->d;       // active Channels
    dataGrid.scanMode = *(long*)precord->e;          // scan mode
#endif

    if (dataGrid.scanMode == LINEAR_SCAN) // Linear mode
    {
        lengthData = dataGrid.pointsSlit * dataGrid.pointsGrid * dataGrid.numChannels;
    }
    else // Precalculated Positions
    {
         lengthData = LengthPointsProcess(a);
    }

    int *a;
    size_t size = 2000*sizeof(int);
    a = (int *) malloc(size);

    if((xData = (double*)malloc(lengthData * sizeof(double))) == NULL)
    {
        printf("Error allocating the storage for the pointer to buffers\n");
        return -1;
    }
    if((xData = (double*)calloc(lengthData * sizeof(double))) == NULL)
    {
        printf("Error allocating the storage for the pointer to buffers\n");
        return -1;
    }
    if((xpData = (double*)calloc(lengthData * sizeof(double))) == NULL)
    {
        printf("Error allocating the storage for the pointer to buffers\n");
    return -1;
    }
    if((avgData = (double*)calloc(dataGrid.numChannels * sizeof(double))) == NULL)
    {
      printf("Error allocating the storage for the pointer to buffers\n");
     return -1;
    }




    memcpy(precord->vala,&xData[0],j);
    memcpy(precord->valb,&xpData[0],j);
    memcpy(precord->valc,&avgData[0],j);
    memcpy(precord->vald,&intensityData[0],1000*sizeof(double));
    memcpy(precord->vale,&arrayAvg[0],dataGrid.numChannels*sizeof(double));
#endif


/*
    memcpy(precord->vala,&xData[0],j);
    memcpy(precord->valb,&xpData[0],j);
    memcpy(precord->valc,&avgData[0],j);
    memcpy(precord->vald,&intensityData[0],1000*sizeof(double));
    memcpy(precord->vale,&arrayAvg[0],dataGrid.numChannels*sizeof(double));



    lengthData = dataGrid.pointsSlit*dataGrid.pointsGrid; // This is only valid for linear scan, because you don't know in the file the total of the points of the file unless the file has a head where is stablished this number.

    if(!IndEMUData)
    {

        if((intensityData = (double*)calloc(SCAN_POINTS_INTENSITY2D, sizeof(double))) == NULL)
        {
            printf("Error allocating the storage for the pointer to buffers\n");
            return -1;
        }
        if((xData = (double*)calloc(lengthData, sizeof(double))) == NULL)
        {
            printf("Error allocating the storage for the pointer to buffers\n");
            return -1;
        }
        if((xpData = (double*)calloc(lengthData, sizeof(double))) == NULL)
        {
            printf("Error allocating the storage for the pointer to buffers\n");
        return -1;
        }
        if((avgData = (double*)calloc(dataGrid.numChannels, sizeof(double))) == NULL)
        {
          printf("Error allocating the storage for the pointer to buffers\n");
         return -1;
        }

        for(i=0; i < SCAN_POINTS_INTENSITY2D; i++)
        {
            if((intensityData[i] = (double*)calloc(1, sizeof(double))) == NULL)
            {
                printf("Error allocating the storage for the buffers\n");
                return -1;
            }
        }

        for(i=0; i < lengthData; i++)
        {
            if((xData[i] = (double*)calloc(1, sizeof(double))) == NULL)
            {
                printf("Error allocating the storage for the buffers\n");
                return -1;
            }
        }

        for(i=0; i < lengthData; i++)
        {
            if((xpData[i] = (double*)calloc(1, sizeof(double))) == NULL)
            {
                printf("Error allocating the storage for the buffers\n");
                return -1;
            }
        }

        for(i=0; i < dataGrid.numChannels; i++)
        {
            if((avgData[i] = (double*)calloc(1, sizeof(double))) == NULL)
            {
                printf("Error allocating the storage for the buffers\n");
                return -1;
            }
        }


    }
*/

#endif
    return 0;

}





/* Register these symbols for use by IOC code: */


#ifndef TEST_PC
epicsRegisterFunction(AllocateMemProcess);
epicsRegisterFunction(LengthPointsInit);
epicsRegisterFunction(LengthPointsProcess);
epicsRegisterFunction(PrecalPositionsInit);
epicsRegisterFunction(PrecalPositionsProcess);
#endif



