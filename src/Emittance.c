// File: Emittance.c
// Author: Idoia Mazkiaran <imazkiaran@essbilbao.org>
//
// Emittance calculation based on the beam current measured on the Grid wires



#include "Emittance.h"
#include "SscanSubRoutines.h"


#ifdef TEST_PC
long EmittanceCalculationInit(struct _ioAssignChannel * precord) {
#else
long EmittanceCalculationInit(aSubRecord *precord) {
#endif

    pEmData = (struct adcData*)pArrayVoltXp;


    return 0;
}



#ifdef TEST_PC
long EmittanceCalculationProcess(struct _ioAssignChannel * precord) {
#else
long EmittanceCalculationProcess(aSubRecord *precord) {
#endif

    int indChannel;
    long numPoints, indPoints, threshold, numUsedPoints;
    double i,j,k,l;
    double xAverage, xpAverage;
    double peakValue, lowLimit;

    double * ptrAvgData;
    double * ptrXData;
    double * ptrXpData;

#ifdef TEST_PC
    numPoints = 100;     // num points grid
    threshold = 40;
#else
//    numPoints = *(long*)precord->a;     // num points
    threshold = *(long*)precord->b;     // % of Peak Value
#endif
    numPoints = totalSamplesRead;




// Peak Value calculation from all the wires

    ptrAvgData = avgData;

    peakValue = *ptrAvgData;
    for (indPoints=0; indPoints < numPoints; indPoints++, ptrAvgData++)
    {
        if (*ptrAvgData > peakValue)
            peakValue = *ptrAvgData;
    }
// Limit of signal value for emittance
    lowLimit = peakValue * threshold * 0.01;

    printf("peakValue %6f \n", peakValue);
    printf("threshold %d \n", threshold);
    printf("lowLimit %6f \n", lowLimit);


// Depreciate points under that value of current

    ptrAvgData = avgData;
    ptrXData = xData;
    ptrXpData = xpData;

    numUsedPoints = numPoints;
    for (indPoints=0, i=0, j=0, l=0; indPoints < numPoints; indPoints++, ptrAvgData++, ptrXData++, ptrXpData++)
    {
       if (*ptrAvgData > lowLimit)
       {
           i += *ptrXData;
           j += *ptrXpData;
           l += *ptrAvgData;
       }
       else
           numUsedPoints--;
   }

    if (numUsedPoints > 0)
    {
        xAverage = i / numUsedPoints;
        xpAverage = j / numUsedPoints;


        printf("xAverage %6f \n", xAverage);
        printf("xpAverage %6f \n", xpAverage);

        dataEmittance.sumCurrentFactor  = l;
        printf("sumCurrentFactor %6f \n", dataEmittance.sumCurrentFactor);


        if (dataEmittance.sumCurrentFactor != 0x00)
        {

            ptrAvgData = avgData;
            ptrXData = xData;
            ptrXpData = xpData;

            for (indPoints=0, i=0, j=0, k=0; indPoints < numPoints; indPoints++, ptrAvgData++, ptrXData++, ptrXpData++)
            {
                 if (*ptrAvgData > lowLimit)
                 {
                    i += (*ptrXData - xAverage) * (*ptrXData - xAverage)*(*ptrAvgData);

                    j += (*ptrXpData - xpAverage) * (*ptrXpData  - xpAverage)*(*ptrAvgData);

                    k += (*ptrXData - xpAverage) *( *ptrXpData - xpAverage)*(*ptrAvgData);
                 }
             }

// Store values in memory
            dataEmittance.xSquaredFactor    = i/l;
            dataEmittance.xpSquaredFactor   = j/l;
            dataEmittance.xXpSquaredFactor  = k/l;

            printf("xSquaredFactor %6f \n", dataEmittance.xSquaredFactor);
            printf("xpSquaredFactor %6f \n", dataEmittance.xpSquaredFactor);
            printf("xXpSquaredFactor %6f \n", dataEmittance.xXpSquaredFactor);

        }

        dataEmittance.Emittance = calculateEmittance();
        dataEmittance.alfaTwiss = alfaTwissParameter();
        dataEmittance.betaTwiss = betaTwissParameter();
        dataEmittance.gammaTwiss= gammaTwissParameter();

    }
    else
    {
        dataEmittance.Emittance = 0x00;
        dataEmittance.alfaTwiss = 0x00;
        dataEmittance.betaTwiss = 0x00;
        dataEmittance.gammaTwiss= 0x00;
    }

#ifdef TEST_PC
#else
    *(long*)precord->vala = dataEmittance.Emittance;
    *(long*)precord->valb = dataEmittance.alfaTwiss;
    *(long*)precord->valc = dataEmittance.betaTwiss;
    *(long*)precord->vald = dataEmittance.gammaTwiss;
#endif

    return 0;

}


double calculateEmittance(void){

    double op1;

    op1 = dataEmittance.xSquaredFactor * dataEmittance.xpSquaredFactor;

    if ((dataEmittance.sumCurrentFactor == 0x00) || (dataEmittance.xXpSquaredFactor > op1))
        dataEmittance.Emittance = 0x00;
    else
        dataEmittance.Emittance = sqrt((op1) / (dataEmittance.sumCurrentFactor * dataEmittance.sumCurrentFactor) - (dataEmittance.xXpSquaredFactor / dataEmittance.sumCurrentFactor));

    return(dataEmittance.Emittance);

//    return (sqrt((pdEmittanceParam->xSquaredFactor * pdEmittanceParam->xpSquaredFactor) / pdEmittanceParam->sumCurrentFactor   - (pdEmittanceParam->xXpSquaredFactor / pdEmittanceParam->sumCurrentFactor)))

}


double alfaTwissParameter(void){

    if ((dataEmittance.sumCurrentFactor == 0x00) || (dataEmittance.Emittance == 0x00))
        dataEmittance.alfaTwiss = 0x00;
    else
        dataEmittance.alfaTwiss = -(dataEmittance.xXpSquaredFactor /  (dataEmittance.sumCurrentFactor * dataEmittance.Emittance));

    return(dataEmittance.alfaTwiss);
}



double betaTwissParameter(void){

    if ((dataEmittance.sumCurrentFactor == 0x00) || (dataEmittance.Emittance == 0x00))
        dataEmittance.betaTwiss = 0x00;
    else
        dataEmittance.betaTwiss = (dataEmittance.xSquaredFactor / (dataEmittance.sumCurrentFactor * dataEmittance.Emittance));

    return(dataEmittance.betaTwiss);
}


double gammaTwissParameter(void){

    if ((dataEmittance.sumCurrentFactor == 0x00) || (dataEmittance.Emittance == 0x00))
        dataEmittance.gammaTwiss = 0x00;
    else
        dataEmittance.gammaTwiss = (dataEmittance.xpSquaredFactor / (dataEmittance.sumCurrentFactor * dataEmittance.Emittance));

    return(dataEmittance.gammaTwiss);
}




#ifndef TEST_PC
epicsRegisterFunction(EmittanceCalculationInit);
epicsRegisterFunction(EmittanceCalculationProcess);
#endif


