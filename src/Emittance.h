// File: Emittance.h
// Author: Idoia Mazkiaran <imazkiaran@essbilbao.org>
//
// Header file for Emittance.c: Emittance calculation based on the beam current measured on the Grid wires


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <math.h>
#include <string.h>
#include <unistd.h>

#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>
#endif

#include "Test.h"


#ifndef TEST_PC
#include <dbDefs.h>
#include <registryFunction.h>
#include <subRecord.h>
#include <aSubRecord.h>
#include <epicsExport.h>
#endif



#define N_EMITTANCE_PARAM   4



typedef struct _paramEmittance {
    double xSquaredFactor;
    double xpSquaredFactor;
    double xXpSquaredFactor;
    double sumCurrentFactor;
    double Emittance;
    double alfaTwiss;
    double betaTwiss;
    double gammaTwiss;
} paramEmittance;


paramEmittance dataEmittance;



double calculateEmittance(void);
double alfaTwissParameter(void);
double betaTwissParameter(void);
double gammaTwissParameter(void);




