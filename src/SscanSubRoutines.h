// File: SscanSubRoutines.h
// Author: Idoia Mazkiaran <imazkiaran@essbilbao.org>
//
// Header file for SscanSubRoutines.c: Functions related to the processing of the acquisition channel acquired in EMU

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <math.h>
#include <string.h>
#include <unistd.h>

#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>
#endif


#include "Test.h"
#define MEM_STORE 1


#ifndef TEST_PC
#include <dbDefs.h>
#include <registryFunction.h>
#include <subRecord.h>
#include <aSubRecord.h>
#include <epicsExport.h>
#endif


// slit distance to grid in um
//#define DISTANCE		350.0
#define DISTANCE_SLIT2GRID	400000.0

// distance between wires in micrometers
#define DISTANCE_WIRES		5.0

// distance between wires in micrometers
#define GRID_RESOLUTION		1.0



// number of wires on the grid
#define WIRE_CNT		24
//#define WIRE_CNT		4
// number of points to collect for each scan
#define WIRE_NELM		200
// number of pixels in the surface plot
#define SURFACE_CNT		10000

/*
#define SCAN_POINTS_GRID 1000
#define SCAN_POINTS_SLIT 100
#define SCAN_POINTS_INTENSITY2D 100000
*/


// TESTS
#define SCAN_POINTS_GRID 100
#define SCAN_POINTS_SLIT 10
#define SCAN_POINTS_INTENSITY2D 1000


#define LINEAR_SCAN 0
#define PREDEFINED_SCAN 1



long Initialized;
long Initialized2;
long InitializedGridPoints;
long InitializedProcessPoints;
long InitializedChannel;

double *pArrayAvg;
double *pArrayX;
double *pArrayVoltXp;

double *pChannel;


typedef struct _gaussData {
    long slitMin;
    long slitMax;
    double slitAmp;
    double slitSigma;
    double slitLoc;
    double slitOff;
    double *slitGauss;

    long gridMin;
    long gridMax;
    double gridAmp;
    double gridSigma;
    double gridLoc;
    double gridOff;
    double *gridGauss;


} gaussData;

gaussData dataTest;



typedef struct _wireDataEMU {
    double measure[WIRE_NELM];
    double avg;
    double xp;
    double x;
} wireDataEMU;


typedef struct _adcData {
    long state;
    long trigger;
    long nSamples;
    int filled;
    long slitP;
    long gridP;

    long pointsSlit;
    long pointsGrid;
    long xSlit;
    wireDataEMU wires[WIRE_CNT];
} adcData;


// Structs for casting data
typedef struct _wireData {
    double x;
    double avg;
    double xp;
} wireData;



typedef struct _EMUData {
    double x;
    wireData wires[WIRE_CNT];
} EMUData;


// Pointers
EMUData *pIndEMUData;



EMUData *pIndGridScan;

double *pCurrentGrid;
double *pPositionGrid;
// Pointers

#ifdef MEM_STORE
// Pointers

double *xData;
double *xpData;
double *avgData;
double *intensityData;

double memAllocate;

#define MASK_MEMXDATA 1
#define MASK_MEMXPDATA 2
#define MASK_MEMAVGDATA 4
#define MASK_MEMINTDATA 8

long memMask;

#else
EMUData IndEMUData[SCAN_POINTS_SLIT*SCAN_POINTS_GRID];

double intensityData[SCAN_POINTS_INTENSITY2D];
double xData[SCAN_POINTS_INTENSITY2D*WIRE_CNT];
double xpData[SCAN_POINTS_INTENSITY2D*WIRE_CNT];
double avgData[SCAN_POINTS_INTENSITY2D*WIRE_CNT];

#endif
double arrayAvg[WIRE_CNT+1];

long totalSamplesRead;

long indexSamples, indexIntensity;

typedef struct _gridSet {
    float slitPosition;
    long countSlit;
    long numChannels;
    long pointsSlit;
    long pointsGrid;
    double distSlitGrid;
    double wiresDistance;
    long scanMode;
    double nTotalSamples;
    double currentGrid[SCAN_POINTS_GRID];
    double positionGrid[SCAN_POINTS_GRID];
} gridSet;

gridSet dataGrid;



adcData *pIndX;
adcData *pIndXp;
EMUData *pEmData;

// Wire Integrity Check
double wireIntegrityData[WIRE_CNT][WIRE_NELM];

long maskWire;









