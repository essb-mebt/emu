// File: SscanMode.h
// Author: Idoia Mazkiaran <imazkiaran@essbilbao.org>
//
// Header file for SscanMode.c: Functions related to the reading of the file for precalculated values for motion in the EMU

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <math.h>
#include <string.h>
#include <unistd.h>

#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach.h>
#endif


#include "Test.h"


#ifndef TEST_PC
#include <dbDefs.h>
#include <registryFunction.h>
#include <subRecord.h>
#include <aSubRecord.h>
#include <epicsExport.h>
#endif


/*
#define SCAN_POINTS_GRID 1000
#define SCAN_POINTS_SLIT 100
*/

double filePointsLength;



/*
// TESTS
#define SCAN_POINTS_GRID 10
#define SCAN_POINTS_SLIT 5
#define SCAN_POINTS_INTENSITY2D 100
*/









